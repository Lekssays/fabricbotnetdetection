#!/bin/bash

set -e

if [ -z $1 ] || [ -z $2 ]; then
    echo "usage: initialize.sh <init channel config (y/n)> <init crypto config (y/n)>"
    exit 1
fi

init_channel_config=$1
init_crypto_config=$2

if [ $init_crypto_config == 'y' ] || [ $init_crypto_config == 'Y' ]; then
    echo "you chose to initialize crypto configuration, are you sure? (y/N)"

    read choice

    if [ $choice != "y" ] && [ $choice != 'Y' ]; then
        echo "exiting..."
        exit 1
    fi
fi

cd bin

if [ $init_crypto_config == 'y' ] || [ $init_crypto_config == 'Y' ]; then
    rm -rf ./network/crypto-config/
    mkdir -p ./network/crypto-config

    ./cryptogen generate --config=../network/crypto-config.yaml --output=../network/crypto-config
fi

if [ $init_channel_config == 'y' ] || [ $init_channel_config == 'Y' ]; then
    rm -rf ./network/config/

    mkdir -p ./network/config

    ./configtxgen -profile OneOrgOrdererGenesis -configPath ../network -outputBlock ../network/config/genesis.block

    ./configtxgen -profile OneOrgChannel -outputCreateChannelTx ../network/config/channel.tx -configPath ../network -channelID mychannel

    ./configtxgen -profile OneOrgChannel -outputAnchorPeersUpdate ../network/config/Org1MSPanchors.tx -channelID mychannel -asOrg Org1MSP -configPath ../network
fi

echo "*******************************************************"
echo "PLEASE NOTE: after cryptogen execution, set in docker-compose.yml the variable FABRIC_CA_SERVER_CA_KEYFILE=/etc/hyperledger/fabric-ca-server-config/XXXXX where XXXXX is the name of the file under ./crypto-config/peerOrganizations/org1.example.com/ca/"
echo "*******************************************************"
echo "to use nodejs application, re-run addToWallet.js and delete the old certificate if necessary"
echo "*******************************************************"
