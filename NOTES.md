## To add members and/or organizations:
* update network/configtx.yaml
* update network/crypto-config.yaml
* update gateway/networkConnection.yaml
* run initialize.sh
* set (in docker-compose.yml) the variable FABRIC_CA_SERVER_CA_KEYFILE=/etc/hyperledger/fabric-ca-server-config/XXXXX where XXXXX is the name of the file under ./crypto-config/peerOrganizations/org1.example.com/ca/"
