#!/bin/bash

set -e

version=$1
if [ -z $1 ]; then
    version=0
fi

docker exec cli peer chaincode install -n BotnetDetectionContract -v $version -p /contract -l java
docker exec cli peer chaincode instantiate -o orderer.example.com:7050 -n BotnetDetectionContract -v $version -l java -c '{"Args":["BotnetDetectionContract:initialize"]}' -C mychannel -P "AND ('Org1MSP.member')"
