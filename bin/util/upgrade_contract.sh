#!/bin/bash

set -e

if [ -z $1 ] || [ -z $2 ]; then
    echo 'usage: upgrade_contract.sh <contract name> <new version> [<cli container name>]'
    exit 1
fi

contract_name=$1
version=$2

cli_container_name=$3
if [ -z $3 ]; then
    cli_container_name='cli'
fi

contract_path=$4
if [ -z $4 ]; then
    contract_path=/contract
fi

language=$5
if [ -z $5 ]; then
    language=java
fi

docker exec $cli_container_name peer chaincode install -n $contract_name -v $version -p $contract_path -l $language
docker exec $cli_container_name peer chaincode upgrade -n $contract_name -v $version -l $language -c '{"Args":[]}' -C mychannel -P "AND ('Org1MSP.member')"
