package com.lucalanda.botnetdetectioncontract.model.state;

import com.lucalanda.botnetdetectioncontract.HostCompressor;
import com.lucalanda.botnetdetectioncontract.Util;
import com.lucalanda.botnetdetectioncontract.model.HostCompressedData;
import com.lucalanda.botnetdetectioncontract.model.SerializableMutualContactGraph;
import com.owlike.genson.Genson;
import main.common.CommunicationCluster;
import main.common.Host;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static com.lucalanda.botnetdetectioncontract.Util.buildSet;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

public class DetectionStateTest {
    private static Genson genson = new Genson();

    @Test
    public void serialization_and_deserialization_work_correctly() {
        List<Host> hosts = getDefaultHosts();
        HostCompressedData originalCompressedHosts = new HostCompressor().compress(hosts);

        Set<String> originalBots = Util.buildSet("123.0.0.1", "124.1.1.0");
        SerializableMutualContactGraph graph = getDefaultSerializableMutualContactGraph();

        DetectionState detectionState = new DetectionState(originalCompressedHosts, graph, originalBots);

        String serialized = genson.serialize(detectionState);

        DetectionState deserialized = genson.deserialize(serialized, DetectionState.class);

        assertThat(deserialized.getCompressedHosts()).isEqualTo(originalCompressedHosts);
        assertThat(deserialized.getBotIps()).isEqualTo(originalBots);
        assertThat(deserialized.getMutualContactGraph()).isEqualTo(graph);
    }

    private SerializableMutualContactGraph getDefaultSerializableMutualContactGraph() {
        String[] edges = {"0,1,0.3"};

        return new SerializableMutualContactGraph(edges);
    }

    private List<Host> getDefaultHosts() {
        Set<String> host1Contacts = buildSet("123.0.0.2", "123.1.0.3");
        Set<String> host2Contacts = buildSet("123.2.0.2");

        Set<CommunicationCluster> host1CommunicationClusters = buildSet(
            new CommunicationCluster(host1Contacts, "tcp", 10, 20)
        );

        Set<CommunicationCluster> host2CommunicationClusters = buildSet(
                new CommunicationCluster(host2Contacts, "udp", 10, 20)
        );

        Host host1 = new Host(false, "123.0.0.1", 0.5f, host1Contacts, host1CommunicationClusters);

        Host host2 = new Host(true, "124.1.1.0", 0.2f, host2Contacts, host2CommunicationClusters);

        return Arrays.asList(host1, host2);
    }

}
