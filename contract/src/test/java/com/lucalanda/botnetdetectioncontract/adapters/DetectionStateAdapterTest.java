package com.lucalanda.botnetdetectioncontract.adapters;

import com.lucalanda.botnetdetectioncontract.HostCompressor;
import com.lucalanda.botnetdetectioncontract.Util;
import com.lucalanda.botnetdetectioncontract.model.HostCompressedData;
import com.lucalanda.botnetdetectioncontract.model.SerializableMutualContactGraph;
import com.lucalanda.botnetdetectioncontract.model.state.DetectionState;
import main.common.Host;
import main.model.DetectionData;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import static com.lucalanda.botnetdetectioncontract.Util.buildSet;
import static java.util.Collections.emptySet;
import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

class DetectionStateAdapterTest {

    private static final DetectionStateAdapter adapter = new DetectionStateAdapter();

    private static final List<Host> hosts = Arrays.asList(
            new Host(false, "123.0.0.1", 0.5f,
                    buildSet("123.0.0.2", "123.0.0.3"), emptySet()),
            new Host(false, "124.1.1.0", 0.2f,
                    buildSet("123.0.0.2"), emptySet())
    );
    private static final HostCompressedData hostCompressedData = new HostCompressor().compress(hosts);

    private static final Set<String> originalBotIps = Util.buildSet("123.0.0.1", "124.1.1.0");
    private static final SerializableMutualContactGraph originalGraph = getDefaultSerializableMutualContactGraph();


    @Test
    void convert() {
        DetectionData detectionData = new DetectionData(hosts, originalGraph, originalBotIps);

        DetectionState detectionState = adapter.convert(detectionData);

        assertThat(detectionState.getCompressedHosts()).isEqualTo(hostCompressedData);
        assertThat(detectionState.getBotIps()).isEqualTo(originalBotIps);
        assertThat(detectionState.getMutualContactGraph()).isEqualTo(originalGraph);
    }

    @Test
    void revert() {
        DetectionState detectionState = new DetectionState(hostCompressedData, originalGraph, originalBotIps);

        DetectionData detectionData = adapter.revert(detectionState);

        assertThat(detectionData.getBotIps()).isEqualTo(originalBotIps);
        assertThat(detectionData.getGraph()).isEqualTo(originalGraph);
    }

    @Test
    void convert_and_revert_work() {
        DetectionState detectionState = new DetectionState(hostCompressedData, originalGraph, originalBotIps);

        DetectionData revertedData = adapter.revert(detectionState);

        DetectionState reconvertedState = adapter.convert(revertedData);

        assertThat(reconvertedState).isEqualTo(detectionState);
    }

    private static SerializableMutualContactGraph getDefaultSerializableMutualContactGraph() {
        String[] edges = {"0,1,0.3"};
        return new SerializableMutualContactGraph(edges);
    }
}