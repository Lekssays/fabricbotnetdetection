package com.lucalanda.botnetdetectioncontract;

import com.lucalanda.botnetdetectioncontract.model.NetworkFlow;
import main.common.CommunicationCluster;
import main.common.Host;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;

import static java.util.stream.Collectors.toSet;
import static java.util.stream.IntStream.range;

public class TestUtils {
    private static Random random = new Random(42);
    private static String[] protocols = new String[]{"tcp", "udp"};


    public static NetworkFlow[] getRandomNetworkFlows(int nRecords) {
        ArrayList<NetworkFlow> networkFlows = new ArrayList<>();

        for(int i = 0; i < nRecords; i++) {
            String ipSource = getRandomIp();
            String ipDestination = getRandomIp();
            int bytesPerPacketIn = getRandomBytesPerPacketValue();
            int bytesPerPacketOut = getRandomBytesPerPacketValue();
            String protocol = getRandomProtocol();

            networkFlows.add(new NetworkFlow(ipSource, ipDestination, protocol, bytesPerPacketIn, bytesPerPacketOut));
        }

        return networkFlows.toArray(new NetworkFlow[nRecords]);
    }

    public static int getRandomBytesPerPacketValue() {
        return random.nextInt(1000);
    }

    public static String getRandomIp() {
        return random.nextInt(100) + "." + random.nextInt(100) + "." + random.nextInt(100) + "." + random.nextInt(100);
    }

    public static String getRandomProtocol() {
        return protocols[random.nextInt(protocols.length)];
    }

    public static List<Host> getRandomHosts(int hostsNumber,
                                      int minContactsNumber, int maxContactsNumber,
                                      int minClustersNumber, int maxClustersNumber,
                                      int minIpPrefixesPerClusterNumber, int maxIpPrefixesPerClusterNumber) {
        List<Host> result = new ArrayList<>(hostsNumber);

        for (int i = 0; i < hostsNumber; i++) {
            boolean isP2P = random.nextBoolean();
            String ip = getRandomIp();
            float destinationDiversityRatio = random.nextFloat();

            int contactsNumber = maxContactsNumber == 0 ? 0 : random.nextInt(maxContactsNumber) + minContactsNumber;
            Set<String> contacts = range(0, contactsNumber)
                    .mapToObj(_i -> getRandomIp()).collect(toSet());

            int clustersNumber = maxClustersNumber == 0 ? 0 : random.nextInt(maxClustersNumber) + minClustersNumber;
            Set<CommunicationCluster> clusters = range(0, clustersNumber)
                    .mapToObj(_i -> getRandomCommunicationCluster(contacts, minIpPrefixesPerClusterNumber, maxIpPrefixesPerClusterNumber)).collect(toSet());

            result.add(new Host(isP2P, ip, destinationDiversityRatio, contacts, clusters));
        }

        return result;
    }

    public static CommunicationCluster getRandomCommunicationCluster(Set<String> contacts, int minIpPrefixesPerClusterNumber, int maxIpPrefixesPerClusterNumber) {
        int ipPrefixesNumber = maxIpPrefixesPerClusterNumber == 0 ?
                maxIpPrefixesPerClusterNumber : random.nextInt(maxIpPrefixesPerClusterNumber) + minIpPrefixesPerClusterNumber;

        List<String> contactsList = new ArrayList<>(contacts);

        Set<String> ipPrefixes = range(0, ipPrefixesNumber)
                .mapToObj(_i -> {
                    String contact = contactsList.get(random.nextInt(contactsList.size()));
                    String[] tokens = contact.split("\\.");

                    return tokens[0] + "." + tokens[1];
                }).collect(toSet());

        String protocol = getRandomProtocol();
        int bytesPerPacketIn = getRandomBytesPerPacketValue();
        int bytesPerPacketOut = getRandomBytesPerPacketValue();

        return new CommunicationCluster(ipPrefixes, protocol, bytesPerPacketIn, bytesPerPacketOut);
    }
}
