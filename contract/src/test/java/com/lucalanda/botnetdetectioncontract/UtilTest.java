package com.lucalanda.botnetdetectioncontract;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Collection;
import java.util.List;

import static com.lucalanda.botnetdetectioncontract.Util.splitStringBySize;
import static java.util.Arrays.asList;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class UtilTest {
    @Test
    public void isInteger_works_with_trailing_zeroes() {
        String timestamp = "1602280800000";

        assertTrue(Util.isInteger(timestamp));
    }

    @Test
    public void isInteger_works_correctly() {
        assertTrue(Util.isInteger("123"));
        assertTrue(Util.isInteger("123098"));

        assertFalse(Util.isInteger("123d"));
        assertFalse(Util.isInteger("123d098"));
    }

    @Test
    public void isInteger_returns_false_for_empty_strings() {
        assertFalse(Util.isInteger(""));
    }

    @Test
    public void repeat_repeats_string_correctly() {
        assertEquals("00000000000000000000", Util.repeat("0", 20));
        assertEquals("01010101010101010101", Util.repeat("01", 10));
        assertEquals("012012012012012", Util.repeat("012", 5));
    }

    @Test
    public void leftPad_works_correctly() {
        assertEquals("00000000000000000000", Util.leftPad("000", 20, "0"));
        assertEquals("000000000000123", Util.leftPad("123", 15, "0"));
    }
    
    @Test
    public void splitStringBySize_works_correctly() {
        String s = "1234567890abcdefghijk";

        Collection<String> actual = splitStringBySize(s, 4);
        List<String> expected = asList("1234", "5678", "90ab", "cdef", "ghij", "k");

        assertEquals(expected, actual);
    }
}
