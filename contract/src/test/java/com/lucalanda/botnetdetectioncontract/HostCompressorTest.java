package com.lucalanda.botnetdetectioncontract;

import com.lucalanda.botnetdetectioncontract.model.HostCompressedData;
import main.common.Host;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.lucalanda.botnetdetectioncontract.TestUtils.getRandomHosts;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;

class HostCompressorTest {

    private HostCompressor compressor = new HostCompressor();

    @Test
    void compress_and_decompress_work_with_empty_lists_in_hosts() {
        List<Host> hostsList1 = getRandomHosts(20,
                0, 0,
                1, 20,
                0, 0);

        List<Host> hostsList2 = getRandomHosts(20,
                10, 10,
                0, 0,
                0, 0);


        assertDoesNotThrow(() -> {
            HostCompressedData compressed = compressor.compress(hostsList1);
            compressor.decompress(compressed);
        });

        assertDoesNotThrow(() -> {
            HostCompressedData compressed = compressor.compress(hostsList2);
            compressor.decompress(compressed);
        });
    }

    @Test
    void compress_and_decompress_work() {
        List<Host> originalHosts = getRandomHosts(5000,
                1, 100,
                1, 100,
                1, 10);

        HostCompressedData compressed = compressor.compress(originalHosts);

        List<Host> decompressed = compressor.decompress(compressed);

        assertEquals(originalHosts, decompressed);
    }

}