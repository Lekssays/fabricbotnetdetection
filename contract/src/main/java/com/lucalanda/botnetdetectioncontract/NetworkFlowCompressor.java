package com.lucalanda.botnetdetectioncontract;

import com.lucalanda.botnetdetectioncontract.model.NetworkFlow;
import com.lucalanda.botnetdetectioncontract.model.NetworkFlowCompressedData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static java.lang.Integer.parseInt;

public class NetworkFlowCompressor {

    public NetworkFlowCompressedData compress(NetworkFlow[] networkFlows) {
        ArrayList<String> ipsList = new ArrayList<>();
        HashMap<String, Integer> ipIndexMap = new HashMap<>();

        ArrayList<Integer> bytesPerPacketValuesList = new ArrayList<>();
        HashMap<Integer, Integer> bytesPerPacketValueIndexMap = new HashMap<>();

        ArrayList<String> protocolsList = new ArrayList<>();
        HashMap<String, Integer> protocolIndexMap = new HashMap<>();

        ArrayList<String> recordsList = new ArrayList<>();

        for (NetworkFlow n : networkFlows) {
            int ipSourceIndex = getIndexAndUpdateMapIfNeeded(ipsList, ipIndexMap, n.getIpSource());
            int ipDestinationIndex = getIndexAndUpdateMapIfNeeded(ipsList, ipIndexMap, n.getIpDestination());

            int protocolIndex = getIndexAndUpdateMapIfNeeded(protocolsList, protocolIndexMap, n.getProtocol());

            int bytesPerPacketInIndex = getIndexAndUpdateMapIfNeeded(bytesPerPacketValuesList, bytesPerPacketValueIndexMap, n.getBytesPerPacketIn());
            int bytesPerPacketOutIndex = getIndexAndUpdateMapIfNeeded(bytesPerPacketValuesList, bytesPerPacketValueIndexMap, n.getBytesPerPacketOut());

            recordsList.add(serializeCompressedRecord(ipSourceIndex, ipDestinationIndex, protocolIndex, bytesPerPacketInIndex, bytesPerPacketOutIndex));
        }

        return new NetworkFlowCompressedData(
                Util.stringListToArray(ipsList),
                Util.intListToArray(bytesPerPacketValuesList),
                Util.stringListToArray(protocolsList),
                Util.stringListToArray(recordsList)
        );
    }

    public NetworkFlow[] decompress(NetworkFlowCompressedData networkFlowCompressedData) {
        ArrayList<NetworkFlow> result = new ArrayList<>();

        for (String record : networkFlowCompressedData.getRecordsList()) {
            result.add(decompressRecord(networkFlowCompressedData, record));
        }

        return result.toArray(new NetworkFlow[result.size()]);
    }


    private NetworkFlow decompressRecord(NetworkFlowCompressedData compressedData, String record) {
        String[] tokens = record.split(",");

        String ipSource = compressedData.getIpsList()[parseInt(tokens[0])];
        String ipDestination = compressedData.getIpsList()[parseInt(tokens[1])];
        String protocol = compressedData.getProtocolsList()[parseInt(tokens[2])];
        int bytesPerPacketIn = compressedData.getBytesPerPacketValuesList()[parseInt(tokens[3])];
        int bytesPerPacketOut = compressedData.getBytesPerPacketValuesList()[parseInt(tokens[4])];

        return new NetworkFlow(ipSource, ipDestination, protocol, bytesPerPacketIn, bytesPerPacketOut);
    }

    private String serializeCompressedRecord(int ipSourceIndex, int ipDestinationIndex, int protocolIndex, int bytesPerPacketInIndex, int bytesPerPacketOutIndex) {
        return ipSourceIndex + "," + ipDestinationIndex + "," + protocolIndex + "," + bytesPerPacketInIndex + "," + bytesPerPacketOutIndex;
    }

    private int getIndexAndUpdateMapIfNeeded(List<Integer> valuesList, HashMap<Integer, Integer> indexMap, int value) {
        int index;
        if (indexMap.containsKey(value)) {
            index = indexMap.get(value);
        } else {
            index = valuesList.size();
            indexMap.put(value, index);
            valuesList.add(value);
        }

        return index;
    }

    private int getIndexAndUpdateMapIfNeeded(List<String> valuesList, HashMap<String, Integer> indexMap, String value) {
        int index;
        if (indexMap.containsKey(value)) {
            index = indexMap.get(value);
        } else {
            index = valuesList.size();
            indexMap.put(value, index);
            valuesList.add(value);
        }

        return index;
    }

}
