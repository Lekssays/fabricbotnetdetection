package com.lucalanda.botnetdetectioncontract.adapters;

import com.lucalanda.botnetdetectioncontract.HostCompressor;
import com.lucalanda.botnetdetectioncontract.model.HostCompressedData;
import com.lucalanda.botnetdetectioncontract.model.SerializableMutualContactGraph;
import com.lucalanda.botnetdetectioncontract.model.state.DetectionState;
import main.common.Host;
import main.model.DetectionData;

import java.util.List;
import java.util.Set;

public class DetectionStateAdapter {
    private final HostCompressor hostCompressor = new HostCompressor();

    public DetectionState convert(DetectionData data) {
        HostCompressedData compressedHosts = hostCompressor.compress(data.getHosts());

        Set<String> botIps = data.getBotIps();
        SerializableMutualContactGraph graph = SerializableMutualContactGraph.from(data.getGraph());

        return new DetectionState(compressedHosts, graph, botIps);
    }

    public DetectionData revert(DetectionState state) {
        List<Host> hosts = hostCompressor.decompress(state.getCompressedHosts());

        return new DetectionData(hosts, state.getMutualContactGraph(), state.getBotIps());
    }

}
