package com.lucalanda.botnetdetectioncontract;

import java.util.*;

public class Util {
    private static final String INT_REGEX = "^\\d+$";

    public static boolean isInteger(String s) {
        return s.matches(INT_REGEX);
    }

    public static String leftPad(String input, int length, String fill) {
        String pad = String.format("%" + length + "s", "").replace(" ", fill) + input.trim();
        return pad.substring(pad.length() - length);
    }

    public static String repeat(String s, int repetitions) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < repetitions; i++) {
            sb.append(s);
        }

        return sb.toString();
    }

    public static List<String> splitStringBySize(String str, int size) {
        ArrayList<String> split = new ArrayList<>();
        for (int i = 0; i <= str.length() / size; i++) {
            split.add(str.substring(i * size, Math.min((i + 1) * size, str.length())));
        }
        return split;
    }

    public static String[] stringListToArray(List<String> list) {
        return list.toArray(new String[list.size()]);
    }

    public static int[] intListToArray(List<Integer> list) {
        int[] result = new int[list.size()];
        int i = 0;
        for(Integer f : list) {
            result[i++] = f;
        }

        return result;
    }

    public static float[] floatListToArray(List<Float> list) {
        float[] result = new float[list.size()];
        int i = 0;
        for(Float f : list) {
            result[i++] = f;
        }

        return result;
    }

    public static <T> Set<T> buildSet(T... params) {
        return new HashSet<>(Arrays.asList(params));
    }

}
