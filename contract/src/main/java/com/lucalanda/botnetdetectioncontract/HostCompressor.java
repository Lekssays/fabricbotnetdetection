package com.lucalanda.botnetdetectioncontract;

import com.lucalanda.botnetdetectioncontract.model.HostCompressedData;
import main.common.CommunicationCluster;
import main.common.Host;

import java.util.*;

import static com.lucalanda.botnetdetectioncontract.Util.*;

public class HostCompressor {
    private static final boolean[] isP2PValues = new boolean[]{false, true};

    public HostCompressedData compress(List<Host> hosts) {
        DataHelper dataHelper = new DataHelper();

        int[][] hostBaseRecords = new int[hosts.size()][];
        int[][] hostContactLists = new int[hosts.size()][];
        int[][][] hostsCommunicationClusters = new int[hosts.size()][][];
        int hostIndex = 0;

        for (Host host : hosts) {
            int isP2PIndex = host.isP2P() ? 1 : 0;
            int ipIndex = getIndexAndUpdateMapIfNeeded(dataHelper.ipValues, dataHelper.ipIndexMap, host.getIp());
            int destinationDiversityRatioIndex = getIndexAndUpdateMapIfNeeded(dataHelper.destinationDiversityRatioValues,
                    dataHelper.destinationDiversityRatioIndexMap, host.getDestinationDiversityRatio());

            int[] baseRecord = compressBaseRecord(isP2PIndex, ipIndex, destinationDiversityRatioIndex);
            int[] contactList = compressContactList(dataHelper.ipValues, dataHelper.ipIndexMap, host);
            int[][] communicationClusters = compressCommunicationClusters(dataHelper, host);

            hostBaseRecords[hostIndex] = baseRecord;
            hostContactLists[hostIndex] = contactList;
            hostsCommunicationClusters[hostIndex] = communicationClusters;
            hostIndex++;
        }

        return new HostCompressedData(isP2PValues, dataHelper.ipsArray(), dataHelper.destinationDiversityRatiosArray(),
                dataHelper.ipPrefixesArray(), dataHelper.protocolsArray(), dataHelper.bytesPerPacketArray(),
                hostBaseRecords, hostContactLists, hostsCommunicationClusters);
    }

    public List<Host> decompress(HostCompressedData data) {
        int hostsNumber = data.getHostBaseRecords().length;
        List<Host> result = new ArrayList<>(hostsNumber);

        for (int i = 0; i < hostsNumber; i++) {
            int[] baseRecord = data.getHostBaseRecords()[i];

            boolean isP2P = isP2PValues[baseRecord[0]];
            String ip = data.getIpValues()[baseRecord[1]];
            float destinationDiversityRatio = data.getDestinationDiversityRatioValues()[baseRecord[2]];

            int[] contactIndexes = data.getHostContactLists()[i];
            Set<String> contacts = decompressContactList(contactIndexes, data);

            int[][] hostCommunicationClusters = data.getHostCommunicationClusters()[i];
            Set<CommunicationCluster> clusters = decompressCommunicationClusters(hostCommunicationClusters, data);

            result.add(new Host(isP2P, ip, destinationDiversityRatio, contacts, clusters));
        }

        return result;
    }

    private Set<String> decompressContactList(int[] contactIndexes, HostCompressedData data) {
        Set<String> result = new HashSet<>();

        for (int index : contactIndexes) {
            result.add(data.getIpValues()[index]);
        }

        return result;
    }

    private int[] compressBaseRecord(int isP2PIndex, int ipIndex, int destinationDiversityRatioIndex) {
        return new int[]{isP2PIndex, ipIndex, destinationDiversityRatioIndex};
    }

    private int[][] compressCommunicationClusters(DataHelper dataHelper, Host host) {
        int[][] result = new int[host.getCommunicationClusters().size()][];

        int i = 0;
        for (CommunicationCluster cluster : host.getCommunicationClusters()) {
            int[] clusterArray = new int[cluster.getIp16Prefixes().size() + 3];

            int protocolIndex = getIndexAndUpdateMapIfNeeded(dataHelper.protocolValues, dataHelper.protocolIndexMap, cluster.getProtocol());
            int bytesPerPacketInValueIndex = getIndexAndUpdateMapIfNeeded(
                    dataHelper.bytesPerPacketValues,
                    dataHelper.bytesPerPacketValueIndexMap,
                    cluster.getBytesPerPacketIn()
            );

            int bytesPerPacketOutValueIndex = getIndexAndUpdateMapIfNeeded(
                    dataHelper.bytesPerPacketValues,
                    dataHelper.bytesPerPacketValueIndexMap,
                    cluster.getBytesPerPacketOut()
            );

            clusterArray[0] = protocolIndex;
            clusterArray[1] = bytesPerPacketInValueIndex;
            clusterArray[2] = bytesPerPacketOutValueIndex;

            int clusterArrayIndex = 3;
            for (String ipPrefix : cluster.getIp16Prefixes()) {
                int ipPrefixIndex = getIndexAndUpdateMapIfNeeded(dataHelper.ipPrefixValues, dataHelper.ipPrefixesIndexMap, ipPrefix);
                clusterArray[clusterArrayIndex++] = ipPrefixIndex;
            }

            result[i++] = clusterArray;
        }

        return result;
    }

    private Set<CommunicationCluster> decompressCommunicationClusters(int[][] hostCommunicationClusters, HostCompressedData data) {
        Set<CommunicationCluster> clusters = new HashSet<>();

        for (int[] serializedCluster : hostCommunicationClusters) {
            String protocol = data.getProtocolValues()[serializedCluster[0]];
            int bytesPerPacketIn = data.getBytesPerPacketValues()[serializedCluster[1]];
            int bytesPerPacketOut = data.getBytesPerPacketValues()[serializedCluster[2]];

            Set<String> ipPrefixes = new HashSet<>();
            for (int i = 3; i < serializedCluster.length; i++) {
                String ipPrefix = data.getIpPrefixValues()[serializedCluster[i]];
                ipPrefixes.add(ipPrefix);
            }

            clusters.add(new CommunicationCluster(ipPrefixes, protocol, bytesPerPacketIn, bytesPerPacketOut));
        }

        return clusters;
    }

    private int[] compressContactList(ArrayList<String> ipsList, HashMap<String, Integer> ipIndexMap, Host host) {
        int[] result = new int[host.getContacts().size()];

        int i = 0;
        for (String contact : host.getContacts()) {
            int index = getIndexAndUpdateMapIfNeeded(ipsList, ipIndexMap, contact);
            result[i++] = index;
        }

        return result;
    }

    private int getIndexAndUpdateMapIfNeeded(List<Float> valuesList, HashMap<Float, Integer> indexMap, float value) {
        int index;
        if (indexMap.containsKey(value)) {
            index = indexMap.get(value);
        } else {
            index = valuesList.size();
            indexMap.put(value, index);
            valuesList.add(value);
        }

        return index;
    }

    private int getIndexAndUpdateMapIfNeeded(List<String> valuesList, HashMap<String, Integer> indexMap, String value) {
        int index;
        if (indexMap.containsKey(value)) {
            index = indexMap.get(value);
        } else {
            index = valuesList.size();
            indexMap.put(value, index);
            valuesList.add(value);
        }

        return index;
    }

    private int getIndexAndUpdateMapIfNeeded(List<Integer> valuesList, HashMap<Integer, Integer> indexMap, int value) {
        int index;
        if (indexMap.containsKey(value)) {
            index = indexMap.get(value);
        } else {
            index = valuesList.size();
            indexMap.put(value, index);
            valuesList.add(value);
        }

        return index;
    }

    private class DataHelper {
        HashMap<String, Integer> ipIndexMap = new HashMap<>();
        ArrayList<String> ipValues = new ArrayList<>();

        HashMap<Float, Integer> destinationDiversityRatioIndexMap = new HashMap<>();
        ArrayList<Float> destinationDiversityRatioValues = new ArrayList<>();

        HashMap<String, Integer> ipPrefixesIndexMap = new HashMap<>();
        ArrayList<String> ipPrefixValues = new ArrayList<>();

        HashMap<String, Integer> protocolIndexMap = new HashMap<>();
        ArrayList<String> protocolValues = new ArrayList<>();

        HashMap<Integer, Integer> bytesPerPacketValueIndexMap = new HashMap<>();
        ArrayList<Integer> bytesPerPacketValues = new ArrayList<>();

        String[] ipsArray() {
            return stringListToArray(ipValues);
        }


        float[] destinationDiversityRatiosArray() {
            return floatListToArray(destinationDiversityRatioValues);
        }

        String[] ipPrefixesArray() {
            return stringListToArray(ipPrefixValues);
        }

        String[] protocolsArray() {
            return stringListToArray(protocolValues);
        }

        int[] bytesPerPacketArray() {
            return intListToArray(bytesPerPacketValues);
        }
    }
}
