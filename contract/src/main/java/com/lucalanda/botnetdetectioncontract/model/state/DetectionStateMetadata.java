package com.lucalanda.botnetdetectioncontract.model.state;

import com.owlike.genson.annotation.JsonProperty;

public class DetectionStateMetadata {
    private final int maxStateNumber;

    public DetectionStateMetadata(@JsonProperty("maxStateNumber") int maxStateNumber) {
        this.maxStateNumber = maxStateNumber;
    }

    public int getMaxStateNumber() {
        return maxStateNumber;
    }
}
