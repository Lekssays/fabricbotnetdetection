/*
 * SPDX-License-Identifier: Apache-2.0
 */

package com.lucalanda.botnetdetectioncontract.model;

import com.owlike.genson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.Objects;

import static java.lang.Integer.parseInt;
import static com.lucalanda.botnetdetectioncontract.Util.isInteger;

public final class NetworkFlow {

    private final String ipSource;

    private final String ipDestination;

    private final int bytesPerPacketIn;

    private final int bytesPerPacketOut;

    private final String protocol;

    public static NetworkFlow[] createInstancesFromCsv(String dataCsv) {
        return createInstancesFromCsv(dataCsv, ",");
    }

    public static NetworkFlow[] createInstancesFromCsv(String dataCsv, String separator) {
        ArrayList<NetworkFlow> result = new ArrayList<>();

        String[] lines = dataCsv.split("\n");

        for (String line : lines) {
            String[] args = line.split(separator);
            String[] trimmedArgs = new String[args.length];

            for (int i = 0; i < args.length; i++) {
                trimmedArgs[i] = args[i].trim();
            }

            if (validArgsSet(trimmedArgs)) {
                result.add(buildNetworkFlow(trimmedArgs));
            }
        }

        return result.toArray(new NetworkFlow[result.size()]);
    }

    public NetworkFlow(@JsonProperty("ipSource") final String ipSource,
                       @JsonProperty("ipDestination") final String ipDestination,
                       @JsonProperty("protocol") final String protocol,
                       @JsonProperty("bytesPerPacketIn") final int bytesPerPacketIn,
                       @JsonProperty("bytesPerPacketOut") final int bytesPerPacketOut) {
        this.ipSource = ipSource;
        this.ipDestination = ipDestination;
        this.protocol = protocol;
        this.bytesPerPacketIn = bytesPerPacketIn;
        this.bytesPerPacketOut = bytesPerPacketOut;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof NetworkFlow && this.hashCode() == obj.hashCode();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getIpSource(), getIpDestination(), getBytesPerPacketIn(), getBytesPerPacketOut(), getProtocol());
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "@" + Integer.toHexString(hashCode())
                + " [ipSource=" + ipSource + ", ipDestination=" + ipDestination + ", protocol=" + protocol
                + ", bytesPerPacketIn=" + bytesPerPacketIn + ", bytesPerPacketOut=" + bytesPerPacketOut + "]";
    }

    public String getIpSource() {
        return ipSource;
    }

    public String getIpDestination() {
        return ipDestination;
    }

    public int getBytesPerPacketIn() {
        return bytesPerPacketIn;
    }

    public int getBytesPerPacketOut() {
        return bytesPerPacketOut;
    }

    public String getProtocol() {
        return protocol;
    }


    private static NetworkFlow buildNetworkFlow(String[] args) {
        return new NetworkFlow(args[0], args[1], args[2], parseInt(args[3]), parseInt(args[4]));
    }

    private static boolean validArgsSet(String[] args) {
        if(args.length != 5) {
            return false;
        }

        for (String arg : args) {
            if(arg.length() == 0) {
                return false;
            }
        }

        return isInteger(args[3]) && isInteger(args[4]);
    }
}
