package com.lucalanda.botnetdetectioncontract.model;

import com.owlike.genson.annotation.JsonProperty;

import java.util.Arrays;

public class HostCompressedData {

    private final boolean[] isP2PValues;
    private final String[] ipValues;
    private final float[] destinationDiversityRatioValues;
    private final String[] ipPrefixValues;
    private final String[] protocolValues;
    private final int[] bytesPerPacketValues;
    private final int[][] hostBaseRecords;
    private final int[][] hostContactLists;
    private final int[][][] hostCommunicationClusters;

    public HostCompressedData(@JsonProperty("isP2PValues") boolean[] isP2PValues,
                              @JsonProperty("ipValues") String[] ipValues,
                              @JsonProperty("destinationDiversityRatioValues") float[] destinationDiversityRatioValues,
                              @JsonProperty("ipPrefixValues") String[] ipPrefixValues,
                              @JsonProperty("protocolValues") String[] protocolValues,
                              @JsonProperty("bytesPerPacketValues") int[] bytesPerPacketValues,
                              @JsonProperty("hostBaseRecords") int[][] hostBaseRecords,
                              @JsonProperty("hostContactLists") int[][] hostContactLists,
                              @JsonProperty("hostCommunicationClusters") int[][][] hostCommunicationClusters) {
        this.isP2PValues = isP2PValues;
        this.ipValues = ipValues;
        this.destinationDiversityRatioValues = destinationDiversityRatioValues;
        this.ipPrefixValues = ipPrefixValues;
        this.protocolValues = protocolValues;
        this.bytesPerPacketValues = bytesPerPacketValues;
        this.hostBaseRecords = hostBaseRecords;
        this.hostContactLists = hostContactLists;
        this.hostCommunicationClusters = hostCommunicationClusters;
    }

    public static HostCompressedData getEmptyData() {
        return new HostCompressedData(new boolean[0], new String[0], new float[0], new String[0], new String[0], new int[0],
                new int[0][], new int[0][], new int[0][][]);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof HostCompressedData)) return false;

        HostCompressedData that = (HostCompressedData) o;

        if (!Arrays.equals(getIsP2PValues(), that.getIsP2PValues())) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getIpValues(), that.getIpValues())) return false;
        if (!Arrays.equals(getDestinationDiversityRatioValues(), that.getDestinationDiversityRatioValues()))
            return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getIpPrefixValues(), that.getIpPrefixValues())) return false;
        // Probably incorrect - comparing Object[] arrays with Arrays.equals
        if (!Arrays.equals(getProtocolValues(), that.getProtocolValues())) return false;
        if (!Arrays.equals(getBytesPerPacketValues(), that.getBytesPerPacketValues())) return false;
        if (!Arrays.deepEquals(getHostBaseRecords(), that.getHostBaseRecords())) return false;
        if (!Arrays.deepEquals(getHostContactLists(), that.getHostContactLists())) return false;
        return Arrays.deepEquals(getHostCommunicationClusters(), that.getHostCommunicationClusters());

    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(getIsP2PValues());
        result = 31 * result + Arrays.hashCode(getIpValues());
        result = 31 * result + Arrays.hashCode(getDestinationDiversityRatioValues());
        result = 31 * result + Arrays.hashCode(getIpPrefixValues());
        result = 31 * result + Arrays.hashCode(getProtocolValues());
        result = 31 * result + Arrays.hashCode(getBytesPerPacketValues());
        result = 31 * result + Arrays.deepHashCode(getHostBaseRecords());
        result = 31 * result + Arrays.deepHashCode(getHostContactLists());
        result = 31 * result + Arrays.deepHashCode(getHostCommunicationClusters());
        return result;
    }

    public boolean[] getIsP2PValues() {
        return isP2PValues;
    }

    public String[] getIpValues() {
        return ipValues;
    }

    public float[] getDestinationDiversityRatioValues() {
        return destinationDiversityRatioValues;
    }

    public String[] getIpPrefixValues() {
        return ipPrefixValues;
    }

    public String[] getProtocolValues() {
        return protocolValues;
    }

    public int[] getBytesPerPacketValues() {
        return bytesPerPacketValues;
    }

    public int[][] getHostBaseRecords() {
        return hostBaseRecords;
    }

    public int[][] getHostContactLists() {
        return hostContactLists;
    }

    public int[][][] getHostCommunicationClusters() {
        return hostCommunicationClusters;
    }
}
