package com.lucalanda.botnetdetectioncontract.model.state;

import com.lucalanda.botnetdetectioncontract.model.HostCompressedData;
import com.lucalanda.botnetdetectioncontract.model.SerializableMutualContactGraph;
import com.owlike.genson.annotation.JsonProperty;
import main.model.MutualContactGraph;

import java.util.Set;

import static com.lucalanda.botnetdetectioncontract.model.SerializableMutualContactGraph.getEmptyGraph;
import static java.util.Collections.emptySet;

public class DetectionState {
    private final HostCompressedData compressedHosts;
    private final MutualContactGraph mutualContactGraph;
    private final Set<String> botIps;

    public DetectionState(@JsonProperty("compressedHosts") HostCompressedData compressedHosts,
                          @JsonProperty("mutualContactGraph") SerializableMutualContactGraph mutualContactGraph,
                          @JsonProperty("botIps") Set<String> botIps) {
        this.compressedHosts = compressedHosts;
        this.mutualContactGraph = mutualContactGraph;
        this.botIps = botIps;
    }

    public static DetectionState getEmptyState() {
        return new DetectionState(HostCompressedData.getEmptyData(), getEmptyGraph(), emptySet());
    }

    @Override
    public boolean equals(Object o) {
        return this.hashCode() == o.hashCode();
    }

    @Override
    public int hashCode() {
        int result = getCompressedHosts().hashCode();
        result = 31 * result + getMutualContactGraph().hashCode();
        result = 31 * result + getBotIps().hashCode();
        return result;
    }

    public MutualContactGraph getMutualContactGraph() {
        return mutualContactGraph;
    }

    public Set<String> getBotIps() {
        return botIps;
    }

    public HostCompressedData getCompressedHosts() {
        return compressedHosts;
    }
}
