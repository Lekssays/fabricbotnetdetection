package com.lucalanda.botnetdetectioncontract;

import com.owlike.genson.Genson;
import org.hyperledger.fabric.shim.ChaincodeStub;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.lucalanda.botnetdetectioncontract.Util.buildSet;

public class BotnetDetectionChaincode extends CustomChaincodeBase {

    private final Genson g = new Genson();

    private final BotnetDetectionContract contract;

    private static final String PERFORM_BOTNET_DETECTION_FUNCTION = "performBotnetDetection";
    private static final String GET_DETECTION_STATE_FUNCTION = "getDetectionState";
    private static final String ADD_SINGLE_NETWORK_FLOW_FUNCTION = "addSingleNetworkFlow";
    private static final String ADD_MULTIPLE_NETWORK_FLOWS_FUNCTION = "addMultipleNetworkFlows";
    private static final String GET_NETWORK_FLOWS_BY_RANGE = "getNetworkFlowsByRange";
    private static final String GET_NETWORK_FLOWS_FOR_KEY = "getNetworkFlowsForKey";

    private static final Map<String, Integer> functionArgumentsSize = new HashMap<String, Integer>() {{
        put(PERFORM_BOTNET_DETECTION_FUNCTION, 2);
        put(GET_DETECTION_STATE_FUNCTION, 0);
        put(ADD_SINGLE_NETWORK_FLOW_FUNCTION, 6);
        put(ADD_MULTIPLE_NETWORK_FLOWS_FUNCTION, 2);
        put(GET_NETWORK_FLOWS_BY_RANGE, 2);
        put(GET_NETWORK_FLOWS_FOR_KEY, 1);
    }};

    public BotnetDetectionChaincode() {
        this.contract = new BotnetDetectionContract();
    }

    @Override
    public Response init(ChaincodeStub chaincodeStub) {
        List<String> args = chaincodeStub.getStringArgs();

        if(args.size() == 1 && args.get(0).equalsIgnoreCase("BotnetDetectionContract:initialize")) {
            contract.initialize(chaincodeStub);
            System.out.println("BotnetDetectionContract has been initialized");
        }

        return new Response(Response.Status.SUCCESS, "Chaincode initialization", "Chaincode has been successfully initialized".getBytes());
    }

    @Override
    public Response invoke(ChaincodeStub chaincodeStub) {
        List<String> args = chaincodeStub.getStringArgs();
        String function = args.get(0);

        String error = checkInvocationError(function, args);
        if (error != null) {
            return new Response(Response.Status.INTERNAL_SERVER_ERROR, "Invalid contract invocation", error.getBytes());
        }

        String response;
        switch (function) {
            case PERFORM_BOTNET_DETECTION_FUNCTION:
                response = g.serialize(contract.performBotnetDetection(chaincodeStub, args.get(1), args.get(2)));
                break;
            case GET_DETECTION_STATE_FUNCTION:
                response = g.serialize(contract.getDetectionState(chaincodeStub));
                break;
            case ADD_SINGLE_NETWORK_FLOW_FUNCTION:
                response = g.serialize(contract.addSingleNetworkFlow(chaincodeStub, args.get(1), args.get(2), args.get(3), args.get(4),
                        args.get(5), args.get(6)));
                break;
            case ADD_MULTIPLE_NETWORK_FLOWS_FUNCTION:
                response = g.serialize(contract.addMultipleNetworkFlows(chaincodeStub, args.get(1), args.get(2)));
                break;
            case GET_NETWORK_FLOWS_BY_RANGE:
                response = g.serialize(contract.getNetworkFlowsByRange(chaincodeStub, args.get(1), args.get(2)));
                break;
            case GET_NETWORK_FLOWS_FOR_KEY:
                response = g.serialize(contract.getNetworkFlowsForKey(chaincodeStub, args.get(1)));
                break;
            default:
                throw new IllegalArgumentException("Unrecognized function invoked \"" + function + "\"");
        }

        return new Response(Response.Status.SUCCESS, "Invocation finished correctly", response.getBytes());
    }

    private String checkInvocationError(String function, List<String> args) {
        Set<String> allowedFunctions = buildSet(
                PERFORM_BOTNET_DETECTION_FUNCTION,
                GET_DETECTION_STATE_FUNCTION,
                ADD_SINGLE_NETWORK_FLOW_FUNCTION,
                ADD_MULTIPLE_NETWORK_FLOWS_FUNCTION,
                GET_NETWORK_FLOWS_BY_RANGE,
                GET_NETWORK_FLOWS_FOR_KEY
        );

        if (!allowedFunctions.contains(function)) {
            return "Invalid function \"" + function + "\" was invoked." + "\n" +
                    "Allowed functions: " + allowedFunctions.toString();
        }

        int expectedArgsSize = functionArgumentsSize.get(function) + 1;
        if (args.size() != expectedArgsSize) {
            return "Wrong number of arguments for function \"" + function + "\": expected: " + expectedArgsSize + ", actual: " + args.size();
        }

        return null;
    }

    public static void main(String[] args) {
        new BotnetDetectionChaincode().start(args);
    }
}
