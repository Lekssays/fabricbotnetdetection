set -e

export COMPOSE_PROJECT_NAME=sample

cd network

docker-compose -f docker-compose.yml up -d ca.example.com orderer.example.com peer0.org1.example.com couchdb cli

cd ../bin/

sleep 5

./create_channel.sh
./join_peer0_to_channel.sh

cd util
./install_contract.sh

echo "Waiting for contract to activate..."
sleep 5

./test_contract_invocation.sh

echo "***** Network up and running *****"
